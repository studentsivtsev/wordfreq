package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task1;

import java.io.*;
import java.util.List;
import java.util.Map;

abstract public class CSVWriter {

    static public int writeCSVfromSLtoFile(List<Map.Entry<String,Integer>> list){

        Writer writer = null;

        try{
            writer = new OutputStreamWriter( new FileOutputStream("outFile"));

            writer.write("Слово,Частота,Частота (в %)\n");
            int countOfWords = 0;
            for (Map.Entry<String,Integer> it: list){
                countOfWords += it.getValue();
            }
            for (Map.Entry<String,Integer> it : list){
                writer.write(it.getKey()+","+it.getValue()+","+it.getValue()*100/countOfWords + "\n");
            }


        }
        catch (IOException e) {
            System.err.println("Error while creating output file: " + e.getLocalizedMessage());
        }
        finally {
            MyFileReader.closeCloseable(writer);
        }

        return 0;
    }
}
