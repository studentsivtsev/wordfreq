package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task1;

import java.io.IOException;
import java.io.Reader;
import java.util.*;


abstract public class Calculator {

    static public List<Map.Entry<String,Integer>> getSortedListFromReader(Reader reader) {

        TreeMap<String, Integer> myMap = new TreeMap<String, Integer>();
        int i;

        try {

            StringBuilder s = new StringBuilder();

            do {
                i = reader.read();
                if (Character.isLetterOrDigit((char) i)) {
                    s.insert(s.length(),(char)i);
                }
                else {
                    String ss = s.toString();

                    if (ss.length() != 0) {
                        if (myMap.containsKey(ss)) {
                            int count = myMap.get(ss);
                            myMap.put(ss, ++count);
                        } else {
                            myMap.put(ss, 1);
                        }
                    }
                    s.delete(0,s.length());
                }
            } while (i != -1);

            List<Map.Entry<String, Integer>> sortedList = new ArrayList(myMap.entrySet());

            Collections.sort(sortedList, new Comparator<Map.Entry<String,Integer>>() {
                @Override
                public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                    return o2.getValue() - o1.getValue();
                }
            });
            return sortedList;

        } catch (IOException e){
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }
        return new ArrayList<>();
    }
}