package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task1;

import java.io.*;

abstract public class MyFileReader {

    static public Reader getReader(String fileName){

        Reader reader = null;

        try{
            reader = new InputStreamReader(new FileInputStream(fileName));
            return reader;
        }
        catch (FileNotFoundException e){
            System.err.println("Error while opening file: " + e.getLocalizedMessage());
        }

        return reader;
    }

    static public int closeCloseable(Closeable file){
        if (file != null){
            try{
                file.close();
            }
            catch (IOException e){
                System.err.println("Error while closing file: " + e.getLocalizedMessage());
            }
        }
        return 0;
    }

}
