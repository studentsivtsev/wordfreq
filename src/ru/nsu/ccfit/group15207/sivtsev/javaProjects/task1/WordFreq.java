package ru.nsu.ccfit.group15207.sivtsev.javaProjects.task1;

import java.io.Reader;
import java.util.List;
import java.util.Map;

public class WordFreq {

    public static void main(String args[]){

        if (args.length != 1){
            System.out.println("Missing file name");
            return;
        }

        String fileName = args[0];

        Reader reader;
        reader = MyFileReader.getReader(fileName);

        List<Map.Entry<String,Integer>> list;
        list = Calculator.getSortedListFromReader(reader);

        CSVWriter.writeCSVfromSLtoFile(list);

        MyFileReader.closeCloseable(reader);

    }


}
